FROM python:3.8.10-alpine3.13

RUN apk update \
    && apk add --no-cache \
        curl

WORKDIR /usr/src/app

COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install --no-warn-script-location -r requirements.txt

COPY hello.py .
COPY run_web.sh .
COPY wsgi.py .

# RUN addgroup -S appgroup \
#     && adduser -S appuser -G appgroup \
#     && chown -R appuser:appgroup .

# USER appuser

RUN ["chmod", "+x", "./run_web.sh"]
